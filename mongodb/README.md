# База данных
## Вход в консоль контейнера
`$ sudo docker exec -it api-database /bin/sh`

# Другое 
- `./mongodata` - том в котором хранятся данные БД
- `./mongo-init.js` - скрипт выполняющий предварительное заполнение БД(например добавлят пользователя БД)
- `./mongod.conf` - файл с настройками БД

### Примечание:
Первичные переменные для монго лежат в файле `.env` - там указан рут пользователь БД, а также имя Базы данных.
Данные парамеры используются для инициализации БД.
В файле `./mongo-init.js` создаётся локальный пользователь для базы данных иницированной в `.env:MONGO_INITDB_DATABASE`
