# Docker compose - запустить сборку
`$ sudo docker-compose -f docker-compose.yml up -d --build`

# Вход в консоль контейнера
`sudo docker exec -it database /bin/sh`

# Другие команды
```
// остановить все контейнеры
$ docker stop $(docker ps -a -q)

// удалить все контейнеры
$ docker rm $(docker ps -a -q)

// Все образы
`docker images`

// удалить образ
$ docker rmi [IMAGE: NAME | ID]

```