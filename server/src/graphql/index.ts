import { makeExecutableSchema } from "graphql-tools";
// const { makeExecutableSchema } = require('graphql-tools')

import typeDefs from "./types/";
// const { typeDefs } = require('./types/')
import resolvers from "./resolvers/";
// const { resolvers } = require('./resolvers/')

const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

export default schema;
