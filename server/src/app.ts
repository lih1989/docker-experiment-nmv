require("dotenv").config();
console.log('ENV_FILE_USAGE: ', process.env.ENV_FILE_USAGE);

if (process.env.ENV_FILE_USAGE) {
  console.log('process.env:');
  console.log('PORT: ', process.env.SERVER_PORT);
} else {
  console.error('ENV VARIABLES NOT FOUND!')
  process.exit(100)
}

import { GraphQLServer, PubSub } from "graphql-yoga";
import mongoose from "mongoose";

import schema from "./graphql/";
import { models } from "./config/db/";

const pubsub = new PubSub();

const {
  SERVER_PORT,
  MONGO_INITDB_DATABASE,
  MONGO_USERNAME,
  MONGO_PASSWORD,
  MONGO_HOST,
  MONGO_PORT
} = process.env

const options = {
  port: SERVER_PORT,
  endpoint: "/graphql",
  subscriptions: "/subscriptions",
  playground: "/playground"
};

const context = {
  models,
  pubsub
};

// Connect to MongoDB with Mongoose.
mongoose
  .connect(
    `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_INITDB_DATABASE}`,
    {
      useCreateIndex: true,
      useNewUrlParser: true
    }
  )
  .then((result) => {
    console.log("MongoDB connected", !!result)
  })
  .catch(err => console.log(err));

const server = new GraphQLServer({
  schema,
  context
});

server.start(options, ({ port }) => {
  console.log(`🚀 Server is running on http://localhost:${port}`);
});
